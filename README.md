```bash
# clean plugings
:PlugClean

# reload vim config after adding Plug modules
:so % 

$ install rust-analyzer binary
https://rust-analyzer.github.io/manual.html#rust-analyzer-language-server-binary

# Install search lib from fzf https://github.com/BurntSushi/ripgrep
sudo zyp in ripgrep

# Install LSP server
npm i -g vscode-langservers-extracted
npm i -g tree-sitter-typescript
:LSPInstall tsserver
:LSPInstall rust_analyzer
```
